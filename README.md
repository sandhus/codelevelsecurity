CodeLevelSecurity
=================

Encrypt and compile the java programs


Following folders are present :
1.	code  :  It contains the source code of project

2. library  :  Contains libraries required to run the project successfully.


Following documents are present :
1.	CodeLevelSecurity_DM.pdf  : It contains the datamodel of the project in pdf format.

2.	CodeLevelSecurity_DM.png  : It contains the datamodel of the project in png format.

3.	CodeLevelSecurity_SRS.pdf : It contains the information regarding project.

4.	CodeLevelSecurity_DFD.pdf : It contains the information regarding Data Flow Diagram of project.

5.	CodeLevelSecurity_Database_Data.sql : It contains the database of the project.

6.	GiveAuthenticationToRemoteUsersInMySQL.doc  : Steps to grant access to  client application to the mysql server on server machine. (This step is optional)


Steps to run the project : 

1.	Install all the required softwares which were used during training.
2.	Create the database by executing the script file. Following command is to be used in the command prompt after taking the control to the folder containing the script file.

mysql -u  root < CodeLevelSecurity_Database_Data.sql

3.	Now open and run the project using NetBeans.

4.	Use following username and password :

username : admin
password : admin

username : employee
password: employee

