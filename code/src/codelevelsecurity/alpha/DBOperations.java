/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package codelevelsecurity.alpha;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * This Class contains methods that are involved in different operations of User
 */
public class DBOperations {

    //----------------------------------------------------------------------
    //                               Login Related
    //----------------------------------------------------------------------
    /**
     * This method is used for user authentication process while Logging in
     *
     * @param userName
     * @param password
     * @return UsermasterBean
     */
    public UsermasterBean authenticateUser(String userName, String password) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        UsermasterBean objBean = null;
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from usermaster where Username = ?");
            pstmt.setString(1, userName);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                if (rs.getString("password").equals(password)) {
                    objBean = new UsermasterBean();
                    objBean.setUserId(rs.getInt("User_ID"));
                    objBean.setUsername(rs.getString("Username"));
                    objBean.setPassword(rs.getString("Password"));
                    objBean.setUserType(rs.getString("User_Type"));
                    objBean.setUserStatus(rs.getString("User_Status"));
                    objBean.setName(rs.getString("Name"));
                    objBean.setContactNumber(rs.getString("Contact_Number"));
                    objBean.setEmail(rs.getString("Email"));
                }
            }
        } catch (Exception e) {
            System.out.println("authenticateUser(String userName, String password) : of DBoperations" + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("authenticateUser(String userName, String password) : of DBoperations" + e);
            }
        }
        return objBean;
    }

    /**
     * This Method is used to get Users Detail According to their username and
     * will return the reference of type UsermasterBean
     *
     * @param userName
     * @return UsermasterBean
     */
    public UsermasterBean getUserDetailByUsername(String userName) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        UsermasterBean objBean = null;
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from usermaster where Username = ?");
            pstmt.setString(1, userName);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                objBean = new UsermasterBean();
                objBean.setUserId(rs.getInt("User_ID"));
                objBean.setUsername(rs.getString("Username"));
                objBean.setPassword(rs.getString("Password"));
                objBean.setUserType(rs.getString("User_Type"));
                objBean.setUserStatus(rs.getString("User_Status"));
                objBean.setEmail(rs.getString("Email"));
                objBean.setName(rs.getString("Name"));
                objBean.setContactNumber(rs.getString("Contact_Number"));
            }
        } catch (Exception e) {
            System.out.println("getUserDetailByUsername(String userName)  of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getUserDetailByUsername(String userName) of DBoperations : " + e);
            }
        }
        return objBean;
    }

    //-------------------------------------------------------------------------
    //                                 UserAccountDetail Related
    //-------------------------------------------------------------------------
    /**
     * This method is used to get list of All Usernames and will return
     * reference of type ArrayList
     *
     * @return ArrayList
     */
    public ArrayList getAllUserNameList() {
        Connection conn = null;
        ArrayList alstUser = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select Username from usermaster");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                alstUser.add(rs.getString("Username"));
            }
        } catch (Exception e) {
            System.out.println("getAllUserList() of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllUserList() of DBoperations : " + e);
            }
        }
        return alstUser;
    }

    /**
     * This method is used to get Details of All Users and will return reference
     * of type ArrayList
     *
     * @return ArrayList
     */
    public ArrayList getAllUserDetailList() {
        Connection conn = null;
        ArrayList alstUser = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select User_ID,Username,Password,User_Type,User_Status,Name ,Email,Contact_Number from usermaster");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                {
                    UsermasterBean objBean = new UsermasterBean();
                    objBean.setUserId(rs.getInt("User_ID"));
                    objBean.setUsername(rs.getString("Username"));
                    objBean.setPassword(rs.getString("Password"));
                    objBean.setUserType(rs.getString("User_Type"));
                    objBean.setUserStatus(rs.getString("User_Status"));
                    objBean.setEmail(rs.getString("Email"));
                    objBean.setName(rs.getString("Name"));
                    objBean.setContactNumber(rs.getString("Contact_Number"));
                    alstUser.add(objBean);
                }
            }
        } catch (Exception e) {
            System.out.println("getAllUserDetailList() of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllUserDetailList() of DBoperations : " + e);
            }
        }
        return alstUser;
    }

    /**
     * This method will get Account Detail of User according to userId and will
     * return reference of type UsermasterBean
     *
     * @param userId
     * @return UsermasterBean
     */
    public UsermasterBean getUserAccountDetailByUserId(int userId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        UsermasterBean objBean = new UsermasterBean();
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select User_ID,Username,Password,User_Type,User_Status,Name, Email,Contact_Number from usermaster where User_Id = ?");
            pstmt.setInt(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                {

                    objBean.setUserId(rs.getInt("User_ID"));
                    objBean.setUsername(rs.getString("Username"));
                    objBean.setPassword(rs.getString("Password"));
                    objBean.setUserType(rs.getString("User_Type"));
                    objBean.setUserStatus(rs.getString("User_Status"));
                    objBean.setEmail(rs.getString("Email"));
                    objBean.setName(rs.getString("Name"));
                    objBean.setContactNumber(rs.getString("Contact_Number"));
                }
            }
        } catch (Exception e) {
            System.out.println("getUserDetailByUserId(int userId) of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getUserDetailByUserId(int userId) of DBoperations : " + e);
            }
        }
        return objBean;
    }

    /**
     * This method is used to get Maximum UserId and will return variable of
     * Integer type
     *
     * @return Integer
     */
    public int getMaxUserId() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int maxUserID = 0;
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select max(User_ID) from usermaster");
            rs = pstmt.executeQuery();

            if (rs.next()) {
                maxUserID = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("getMaxUserId() of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getMaxUserId() of DBoperations : " + e);
            }
        }
        return maxUserID;
    }

    /**
     * This method is used to insert new User detail in database and will return
     * variable of type String
     *
     * @param objBean
     * @return String
     */
    public String addUserAccountDetail(UsermasterBean objBean) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String result = "failed";
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from usermaster where Username = ?");
            pstmt.setString(1, objBean.getUsername());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                result = "exists";
            } else {

                pstmt = conn.prepareStatement("insert into usermaster ( User_ID ,Username ,Password,User_Type, User_Status ,Name , Email , Contact_Number) values(?,?,?,?,?,?,?,?) ");
                pstmt.setInt(1, objBean.getUserId());
                pstmt.setString(2, objBean.getUsername());
                pstmt.setString(3, objBean.getPassword());
                pstmt.setString(4, objBean.getUserType());
                pstmt.setString(5, objBean.getUserStatus());
                pstmt.setString(6, objBean.getName());
                pstmt.setString(7, objBean.getEmail());
                pstmt.setString(8, objBean.getContactNumber());

                System.out.println(pstmt.toString());
                int i = pstmt.executeUpdate();
                if (i > 0) {
                    result = "added";
                }
            }
        } catch (Exception e) {
            System.out.println("addUserAccountDetail(UsermasterBean objBean) of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("addUserAccountDetail(UsermasterBean objBean) of DBoperations : " + e);
            }
        }
        return result;
    }

    /**
     * This method is used to update User's Detail and will return variable of
     * type String
     *
     * @param objBean
     * @return String
     */
    public String updateUserAccountDetail(UsermasterBean objBean) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String result = "failed";
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from usermaster where Username = ? and User_ID !=?");
            pstmt.setString(1, objBean.getUsername());
            pstmt.setInt(2, objBean.getUserId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                result = "exists";
            } else {
                pstmt = conn.prepareStatement("update usermaster set Username = ?,Password=?,User_Type =?, User_Status = ? ,Name = ?, Email = ?, Contact_Number = ? where User_ID=?");
                pstmt.setString(1, objBean.getUsername());
                pstmt.setString(2, objBean.getPassword());
                pstmt.setString(3, objBean.getUserType());
                pstmt.setString(4, objBean.getUserStatus());
                pstmt.setString(5, objBean.getName());
                pstmt.setString(6, objBean.getEmail());
                pstmt.setString(7, objBean.getContactNumber());
                pstmt.setInt(8, objBean.getUserId());
                System.out.println(pstmt.toString());
                int i = pstmt.executeUpdate();
                if (i > 0) {
                    result = "updated";
                }
            }
        } catch (Exception e) {
            System.out.println("updateUserAccountDetail(UsermasterBean objBean) of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("updateUserAccountDetail(UsermasterBean objBean) of DBoperations : " + e);
            }
        }
        return result;
    }

    /**
     * This method is used to insert or update Program detail
     *
     * @param objBean
     */
    public void insertUpdateProgramDetail(ProgramUpdateMasterBean objBean) {


        Connection conn = null;
        Statement stmt = null;
        try {
            conn = DBConnection.getConnection();
            stmt = conn.createStatement();
            String strQuery = "insert into programupdatemaster (Program_ID, Update_Date) values ('" + objBean.getProgram_ID() + "','" + objBean.getUpdate_Date() + "')";
            int i = stmt.executeUpdate(strQuery);
            if (i > 0) {
            }
        } catch (Exception e) {
            System.out.println("Exception in insertUpdateProgramDetail DBOperations : " + e);
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in insertUpdateProgramDetail DBOperations finally" + e);
            }
        }

    }

    /**
     * This method is used to insert Detail of Saved Program and returns an
     * integer value
     *
     * @param objBean
     * @return Integer
     */
    public int insertSavedProgramDetail(ProgramMasterBean objBean) {
        int programId = 0;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DBConnection.getConnection();
            //  String programPath = objBean.getProgram_Path(); //Linux
            //String programPath = objBean.getProgram_Path().replace('\\', '/'); //Windows
            pstmt = conn.prepareStatement("insert into programmaster (User_ID, Program_Name,"
                    + " Program_Path, Create_Date) values (?,?,?,?) ");
            pstmt.setInt(1, objBean.getUser_ID());
            pstmt.setString(2, objBean.getProgram_Name());
            pstmt.setString(3, objBean.getProgram_Path());
            pstmt.setString(4, getCurrentDateTime());

            int i = pstmt.executeUpdate();
            if (i > 0) {
                String strQuery1 = "select max(program_id) from programmaster";
                rs = pstmt.executeQuery(strQuery1);
                if (rs.next()) {
                    programId = rs.getInt(1);
                }

            }
        } catch (Exception e) {
            System.out.println("Exception in insertSavedProgramDetail DBOperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in insertSavedProgramDetail DBOperations finally " + e);
            }
        }
        return programId;
    }

    /**
     * This method is used to get Detail of Saved Program
     *
     * @param programPath
     * @return ProgramMasterBean
     */
    public ProgramMasterBean getSavedProgramDetailByPath(String programPath) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ProgramMasterBean objBean = null;
        try {

            objBean = new ProgramMasterBean();
            objBean.setUser_ID(-1);
            objBean.setProgram_ID(-1);
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select Program_ID, User_ID, Program_Name, Program_Path, Create_Date from programmaster where program_path=?");
            pstmt.setString(1, programPath);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                objBean.setProgram_ID(rs.getInt("Program_ID"));
                objBean.setProgram_Name(rs.getString("Program_Name"));
                objBean.setProgram_Path(rs.getString("Program_Path")); // Windos
                objBean.setUser_ID(rs.getInt("User_ID"));
                objBean.setCreate_Date(rs.getString("Create_Date"));
            }
        } catch (Exception e) {
            System.out.println("Exception in getSavedProgramDetailByPath DBOperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in getSavedProgramDetailByPath DBOperations finally " + e);
            }
        }
        return objBean;
    }

    /**
     * This method is used to insert detail of accessed programs
     *
     * @param objBean
     */
    public void insertAccessedProgramDetail(ProgramAccessMasterBean objBean) {


        Connection conn = null;
        Statement stmt = null;
        try {
            conn = DBConnection.getConnection();
            stmt = conn.createStatement();
            String strQuery = "insert into programaccessmaster (Program_ID, User_ID, Access_Date) values ('" + objBean.getProgram_ID() + "','" + objBean.getUser_ID() + "','" + objBean.getAccess_Date() + "')";
            int i = stmt.executeUpdate(strQuery);
            if (i > 0) {
            }
        } catch (Exception e) {
            System.out.println("Exception in insertAccessedProgramDetail DBOperations : " + e);
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in insertAccessedProgramDetail DBOperations finally" + e);
            }
        }

    }

    /**
     * this method is used to get List of Usernames of All Users and returns
     * reference of type ArrayList
     *
     * @return ArrayList
     */
    public ArrayList getUsernameList() {
        ArrayList al = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = DBConnection.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select UserName from usermaster");

            while (rs.next()) {
                al.add(rs.getString("UserName"));
            }
        } catch (Exception e) {
            System.out.println("Exception in DBOperations getUsernameList()" + e);
        } finally {
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in DBOperations getUsernameList() finally " + e);
            }
        }
        return al;
    }

    /**
     * This method is used to get Detail of all Updated Programs and returns
     * reference of type ArrayList
     *
     * @return ArrayList
     */
    public ArrayList getUpdatedProgramReportList() {
        ArrayList alPrograms = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = DBConnection.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select pum.Program_ID, pum.Update_Date, pm.User_ID, pm.Program_Name, pm.Create_Date from programupdatemaster as pum , programmaster pm where pum.program_id=pm.program_id");
            while (rs.next()) {
                ProgramUpdateMasterBean objBean = new ProgramUpdateMasterBean();
                objBean.setProgram_ID(rs.getInt("Program_ID"));
                objBean.setUser_ID(rs.getInt("User_ID"));
                objBean.setProgram_Name(rs.getString("Program_Name"));
                objBean.setUpdate_Date(rs.getString("Update_Date"));
                objBean.setCreate_Date(rs.getString("Create_Date"));
                alPrograms.add(objBean);

            }

        } catch (Exception e) {
            System.out.println("Exception in getUpdatedProgramReportList DBOperations : " + e);
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in getUpdatedProgramReportList DBOperations finally " + e);
            }
        }
        return alPrograms;
    }

    /**
     * This method is used to get Detail of Updated Programs of a User and
     * returns reference of type ArrayList
     *
     * @param username
     * @return ArrayList
     */
    public ArrayList getUpdatedProgramReportList(String username) {
        ArrayList alPrograms = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = DBConnection.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select pum.Program_ID, pum.Update_Date, pm.User_ID, pm.Program_Name, pm.Create_Date from programupdatemaster as pum , programmaster pm ,usermaster um where pum.program_id=pm.program_id and pm.user_id=um.user_id and um.username='" + username + "'");
            while (rs.next()) {
                ProgramUpdateMasterBean objBean = new ProgramUpdateMasterBean();
                objBean.setProgram_ID(rs.getInt("Program_ID"));
                objBean.setUser_ID(rs.getInt("User_ID"));
                objBean.setProgram_Name(rs.getString("Program_Name"));
                objBean.setUpdate_Date(rs.getString("Update_Date"));
                objBean.setCreate_Date(rs.getString("Create_Date"));
                alPrograms.add(objBean);

            }

        } catch (Exception e) {
            System.out.println("Exception in getUpdatedProgramReportList DBOperations : " + e);
        } finally {
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in getUpdatedProgramReportList DBOperations finally " + e);
            }
        }
        return alPrograms;
    }

    /**
     * This method is used to get Detail of All Saved Programs and returns
     * reference of type ArrayList
     *
     * @return ArrayList
     */
    public ArrayList getAcessedProgramReportList() {
        ArrayList alPrograms = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = DBConnection.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT p.`Program_ID`, p.`User_ID` as creater, p.`Program_Name`, p.`Create_Date`, pa.`Access_Date`, pa.`User_ID` as viewer FROM programmaster p, programaccessmaster pa WHERE pa.Program_ID=p.Program_ID;");
            while (rs.next()) {
                ProgramAccessMasterBean objBean = new ProgramAccessMasterBean();
                objBean.setProgram_ID(rs.getInt("Program_ID"));
                objBean.setUser_ID(rs.getInt("creater"));
                objBean.setProgram_Name(rs.getString("Program_Name"));
                objBean.setAccess_Date(rs.getString("Access_Date"));
                objBean.setCreate_Date(rs.getString("Create_Date"));
                objBean.setViewer_User_ID(rs.getInt("viewer"));
                alPrograms.add(objBean);

            }

        } catch (Exception e) {
            System.out.println("Exception in getAcessedProgramReportList DBOperations : " + e);
        } finally {
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in getAcessedProgramReportList DBOperations finally " + e);
            }
        }
        return alPrograms;
    }

    /**
     * This method is used to get Detail of Accessed Programs Corresponding to a
     * User and returns reference of type ArrayList
     *
     * @param username
     * @return ArrayList
     */
    public ArrayList getAcessedProgramReportListByUsername(String username) {
        ArrayList alPrograms = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = DBConnection.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT p.`Program_ID`, p.`User_ID` as creater, p.`Program_Name`, p.`Create_Date`, pa.`Access_Date`, pa.`User_ID` as viewer FROM programmaster p, programaccessmaster pa, usermaster u WHERE pa.Program_ID=p.Program_ID AND u.`Username`='" + username + "' AND pa.`User_ID`= u.`User_ID`");
            while (rs.next()) {
                ProgramAccessMasterBean objBean = new ProgramAccessMasterBean();
                objBean.setProgram_ID(rs.getInt("Program_ID"));
                objBean.setUser_ID(rs.getInt("creater"));
                objBean.setProgram_Name(rs.getString("Program_Name"));
                objBean.setAccess_Date(rs.getString("Access_Date"));
                objBean.setCreate_Date(rs.getString("Create_Date"));
                objBean.setViewer_User_ID(rs.getInt("viewer"));
                alPrograms.add(objBean);
            }

        } catch (Exception e) {
            System.out.println("Exception in getAcessedProgramReportList DBOperations : " + e);
        } finally {
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in getAcessedProgramReportList DBOperations finally" + e);
            }
        }
        return alPrograms;
    }

    /**
     * This method is used to get Detail of All Saved Programs and returns
     * reference of type ArrayList
     *
     * @return ArrayList
     */
    public ArrayList getSavedProgramList() {
        ArrayList alPrograms = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = DBConnection.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select Program_ID, User_ID, Program_Name, Program_Path, Create_Date from programmaster");
            while (rs.next()) {
                ProgramMasterBean objBean = new ProgramMasterBean();
                objBean.setProgram_ID(rs.getInt("Program_ID"));
                objBean.setUser_ID(rs.getInt("User_ID"));
                objBean.setProgram_Name(rs.getString("Program_Name"));
                objBean.setProgram_Path(rs.getString("Program_Path"));
                objBean.setCreate_Date(rs.getString("Create_Date"));
                alPrograms.add(objBean);

            }

        } catch (Exception e) {
            System.out.println("Exception in getSavedProgramList DBOperations : " + e);
        } finally {
            try {
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in getSavedProgramList DBOperations finally" + e);
            }
        }
        return alPrograms;
    }

    /**
     * This method is used to get Details of Saved Programs of a User
     *
     * @param username
     * @return ArrayList
     */
    public ArrayList getSavedProgramListByUsername(String username) {
        ArrayList alPrograms = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = DBConnection.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select Program_ID, pm.User_ID, Program_Name, Program_Path, Create_Date from programmaster pm,usermaster um where um.user_id=pm.user_id and um.username='" + username + "'");
            while (rs.next()) {
                ProgramMasterBean objBean = new ProgramMasterBean();
                objBean.setProgram_ID(rs.getInt("Program_ID"));
                objBean.setUser_ID(rs.getInt("User_ID"));
                objBean.setProgram_Name(rs.getString("Program_Name"));
                objBean.setProgram_Path(rs.getString("Program_Path"));
                objBean.setCreate_Date(rs.getString("Create_Date"));
                alPrograms.add(objBean);

            }

        } catch (Exception e) {
            System.out.println("Exception in getSavedProgramListByUsername DBOperations : " + e);
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("Exception in getSavedProgramListByUsername DBOperations finally" + e);
            }
        }
        return alPrograms;
    }

    /**
     * This method is used to get an Email of a User
     *
     * @param username
     * @return String
     */
    public String getEmailByUsername(String username) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String result = "failed";

        try {


            conn = DBConnection.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery("select email from usermaster um,userpersonaldetail upd where um.user_id=upd.user_id and um.username='" + username + "'");

            while (rs.next()) {
                result = rs.getString("email");
            }





        } catch (Exception e) {

            System.out.println("in getEmailByUsername(username) in dboperations" + e);
            return result;
        } finally {
            try {

                rs.close();
                stmt.close();
                conn.close();

            } catch (Exception e) {

                System.out.println("in getEmailByUsername(username) in dboperations finally" + e);
                return result;
            }
        }

        return result;


    }

    //---------------------------------------------------------------------------------------
    //          Common Methods
    //-----------------------------------------------------------------------------------------------
    /**
     * This method is used to get Current Date and Time Value of a System
     *
     * @return String
     */
    public String getCurrentDateTime() {
        java.util.Date dd = new java.util.Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

        String strDate = sdf.format(dd);
        return strDate;
    }
}
