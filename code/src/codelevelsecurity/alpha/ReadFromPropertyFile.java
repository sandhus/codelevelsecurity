package codelevelsecurity.alpha;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadFromPropertyFile {

    Properties objProp;
    String path;
    File f;

    public String getPath() {
        return objProp.getProperty("path");
    }

    public ReadFromPropertyFile() {
        objProp = new Properties();
        try {
            f = new File("Configuration.properties");
            objProp.load(new FileInputStream(f));
        } catch (Exception e) {
            System.out.println("Properties File" + e);
        }

    }
}
